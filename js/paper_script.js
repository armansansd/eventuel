// $(document).ready(function() {
	var p;

	tool.minDistance = 15;

	function onMouseDown(event) {
		if (p) {
			p.selected = false;
		};
		p = new Path();
		p.strokeColor = 'black';
		p.fullySelected = false;
		p.style = {
	    strokeColor: 'black',
	    strokeWidth: 45
		}
	}

	function onMouseDrag(event) {
		p.add(event.point);
	}

	function onMouseUp(event) {
		p.selected = false;
		p.smooth('catmull-rom');
	}

// });
