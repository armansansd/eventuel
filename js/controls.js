// Array d'alphabet //
var lettres = ["A", "a", "B", "b", "C", "c", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "I", "i", "J", "j", "K", "k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z"];

$(document).ready(function() {


  // États de base de l'interface//
  $("#swDessin").addClass('activemode');

  //


  // i0 = texte d'intro
  // i1 = interface dessin
  // i2 = interface écriture
  // i3 = liste des lettres

  // Clic sur le bouton dessiner d'une lettre de la liste //
  $('body').on('click', '.bouttonDessin', function() {


    $("#propositions").toggleClass("letter-active");

    var dtL = $(this).attr("data-lettre");

    var thisli = $(this).parent();
    var nextli = thisli.next();

    clearCanvas();
    applyName(dtL);
    couleurLettre(dtL);

    if (nextli.is(':visible')) {
      $(".i1").fadeOut();
      $(".i0").fadeIn();
      nextli.slideUp();
      $("#sommaireLettre span").removeClass('focusOn');
    } else {
      if ($('.i1').is(':not(:visible)')) {
        $(".i0").fadeOut();
        $(".i1").fadeIn();
      }

      $(".infoscomp").slideUp();
      nextli.slideDown();
      $("#name").focus();
      $("#lettreTrans").html(dtL);

      setTimeout(function() {
        $([document.documentElement, document.body]).animate({
          scrollTop: thisli.offset().top - $('#pageDeTitre').height() + 1
        }, 1000);
      }, 500);
    }
  });


  // sommaire lettre en haut //

  // déplacement dans la page //
  $('body').on('click', '#sommaireLettre span', function() {
    //efface le dessin non enregistré
    clearCanvas();
    $("#swDessin").addClass('activemode');
    $("#swEcrire").removeClass('activemode');
    $("#download-svg").removeClass('bouttonecriture');

    applyName($(this).attr("data-lettre"));

    var thisli = $(".bouttonDessin[data-lettre='" + $(this).attr("data-lettre") + "']").parent();
    var nextli = thisli.next();

    if (nextli.is(':visible')) {
      $(".i1").fadeOut();
      $(".i0").fadeIn();
      nextli.slideUp();
      $("#sommaireLettre span").removeClass('focusOn');
    } else {
      if ($('.i1').is(':not(:visible)')) {
        if ($('.i2').is(':visible')){
          $("#listecommentaire").fadeOut();
          $(".i0").fadeOut();
          $(".i1").fadeIn();
        }else{
          $(".i0").fadeOut();
          $(".i1").fadeIn();
        }
      }

      $(".infoscomp").slideUp();
      nextli.slideDown();
      $("#name").focus();
      $("#lettreTrans").html($(this).attr("data-lettre"));
      $("#sommaireLettre span").removeClass('focusOn');
      $("span[data-lettre='" + $(this).attr("data-lettre") + "']").addClass('focusOn');

      setTimeout(function() {
        $([document.documentElement, document.body]).animate({
          scrollTop: thisli.offset().top - $('#pageDeTitre').height() + 1
        }, 1000);
      }, 500);

    }
  });


  // animation du canvas lors du survol de la souris //

  $(document).on({
    mouseenter: function() {
      if ($('#canvas').is(':visible')) {
        $("#lettreTrans, #introLettreTrans").css({
          "opacity":"0.3",
          "z-index":"-3"
        });
      }
    },
    mouseleave: function() {
      if ($('#canvas').is(':visible')) {
        $("#lettreTrans, #introLettreTrans").css({
          "opacity":"1",
          "z-index":"0"
        });
      }
    }
  }, '#dessin ');



  // uploader les svg //
  $(".download-svg").click(function() {
    var drawing = new Object();

    var letter = $(this).attr("data-letter");
    console.log(letter);
    var type  = $(this).attr("data-type");
    if ($("#commentaire").is(':visible')) {
       var com  = $("#commentaire").val();
       drawing.com = com;
    }
    var type  = $(this).attr("data-type");

    var date = Date.now();
    // console.log($("#name"))
    var svg = paper.project.exportSVG({
      asString: true
    });

    drawing.name = $("#name")[0].value;
    drawing.date = date;
    drawing.letter = letter;
    drawing.type = type;
    drawing.path = svg;


    if (drawing.name.trim() != "") {
      $.ajax({
          method: "POST",
          url: "../core/upload.php",
          data: drawing,
          success: function(data) {
            console.log(data)
            alert("Merci pour votre contribution !");
            $("#name")[0].value = "";
            $("#commentaire")[0].value = "";
            clearCanvas();
             location.reload();
          },
          error: function(data) {
               console.log(data);
           }
        })
        .fail(function(error) {
          console.log(error);
        });
    } else {
      alert("N'oubliez pas d'indiquer votre nom.");
    }
  });

  // effacer les traits //
  $("#erase").click(function() {
    erase()
  });

  $(document).keydown(function(e) {
    if (e.ctrlKey && e.keyCode == 90) {
      erase();
    }
  });

  function erase() {
    var l = paper.project._activeLayer._children.length;
    console.log(paper.project._activeLayer._children[l - 1].remove());
  }


  // tout effacer dans le canvas//
  function clearCanvas() {
    var project = paper.project;
    if (project != null) {
      var layer = project._activeLayer;
      if (layer != null) {
        $(paper.project._activeLayer._children).each(function() {
          this.remove();
        });
      }
    }else{
      return false;
    }
  }



  $('#sommaireLettre').each(function(index) {
    var characters = $(this).text().split("");

    $this = $(this);
    $this.empty();
    $.each(characters, function(i, el) {
      if (el == " ") {
        $this.append(" ");
      } else {
        $this.append("<span data-lettre=" + el + ">" + el + "</span");
      }
    });
   });


    // intro website //

    $(".infoscomp").each(function() {
      $(this).toggle();
    });

    $('body').on('click', '#popEntree span:first', function() {
      opening();
    });

    $('body').on('click', '#randomlettre', function() {
      rL=randomLettre();
      $('#popEntree').fadeOut();
      var thisli = $(".bouttonDessin[data-lettre='" + rL + "']").parent();
      applyName(rL);
      var nextli = thisli.next();


      $(".i0").fadeOut();
      $(".i1").fadeIn();

      $(".infoscomp").slideUp();
      nextli.slideDown();
      $("#name").focus();
      $("#lettreTrans").html(rL);
      $("#sommaireLettre span").removeClass('focusOn');
      $("span[data-lettre='" + rL + "']").addClass('focusOn');

      setTimeout(function() {
        $([document.documentElement, document.body]).animate({
          scrollTop: thisli.offset().top - $('#pageDeTitre').height() + 1
        }, 1000);
      }, 500);

    });

    $('body').on('click', '#swEcrire', function() {
      $("#swDessin").removeClass('activemode');
      $("#swEcrire").addClass('activemode');
      $("#propositions").removeClass("letter-active");
      $("#download-svg").addClass('bouttonecriture');

      if ($("canvas").is(':visible')){
        $(".i1, .i3").fadeOut();
        $(".i2").fadeIn();
        $('.infoscomp').slideUp();
        $("#sommaireLettre span").removeClass('focusOn');
      }else{
        $(".i0, .i3").fadeOut();
        $(".i2").fadeIn();
      }
      $("html, body").animate({
        scrollTop: 0
      }, 0);
    });

    $('body').on('click', '#swDessin', function() {
      clearCanvas();

      $("#swDessin").addClass('activemode');
      $("#swEcrire").removeClass('activemode');

      $("#download-svg").removeClass('bouttonecriture');
      if ( $("#propositions").hasClass("letter-active") != true) {
        $(".i0").fadeIn();
      }
      $(".i3").fadeIn();
      $(".i2").fadeOut();
    });



    if (getCookie("visit") != "true") {
      document.cookie = "visit=true";
    }else{
      setTimeout(opening, 500);
    }

});

function randomLettre(){
  var rL = lettres[Math.floor(Math.random() * lettres.length)];
  return rL;
}

function couleurLettre(lettre){
  $("#sommaireLettre span").removeClass('focusOn');
  $("span[data-lettre='" + lettre + "']").addClass('focusOn');
}

function applyName(value){
  $(".download-svg").attr("data-letter", value);
}
function opening(){
  var canvas = document.getElementById('canvas');
  paper.setup(canvas);
  
  $("canvas").addClass("notRightnow");
  $('.interface').hide();
  $('.i0, .i3').fadeIn();
  $('#popEntree').fadeOut();


  $("html, body").animate({
    scrollTop: 0
  }, 0);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}
