<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>L'Éventuel, Exploring the future of typographic forms.</title>
  <script src="../js/src/jquery-3.3.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/8.15.0/lazyload.min.js"></script>
  <script src="../js/src/paper-full.min.js"></script>
  <link rel="stylesheet" href="../css/190204_style.css">
  <script type="text/paperscript" canvas="canvas" src="../js/paper_script.js"></script>
  <script type="text/javascript" src="../js/controls.js"></script>
</head>
<body>
  <?php require("../core/toolkit.php"); ?>
  <div id="popEntree">
  <div id="grosTitre">L'Éventuel</div><br><br><br>
  Contribute to the creation of a new typeface, Villa Eventual, by imagining future potential steps of our alphabet’s evolution. Select letters in the list and start drawing using the cursor of your mouse.
  <br><br>
  <span>Start exploring the interface</span>   or  <span id="randomlettre">Start with a random letter</span><br>
  <span><a style="color:inherit; text-decoration:none" href="https://www.villagillet.net/contact">Subscribe to the newsletter</a></span><br>
  <div id="langue"><a href="../fr">Aller à l'interface en français</a></div>
  </div>

<main>
<section id='pageDeTitre'>
  <div id="container">
    <div id="grandTitre">L'Éventuel</div>
    <div id="petitTitre"><span>Exploration into the future of typographic forms</span></div>
    <div id="temps">
      <div id="sommaireLettre">Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz</div>
    </div>
    <div id="swDessin">
      <span>Draw</span>
    </div>
    <div id="swEcrire">
      <span>Comment</span>
    </div>
  </div>
</section>


<section id="dessin">
  <div id="innerDessin">
    <input id="name" type="text" placeholder="Enter your name here" class="interface i1 i2">
    <div class="interface i1" id="lettreTrans">W</div>
    <div class="interface i1" id="introLettreTrans">Draw here with your mouse cursor.</div>
    <canvas id="canvas" class="interface i1" resize></canvas>

    <button data-letter="a" data-type="letter" class="download-svg interface i1 ">Send</button>
    <button data-letter="a" data-type="comz" class="download-svg interface i2 com">Send</button>

    <button id="erase" class="interface i1">Delete last stroke</button>

    <div id="explication"class="interface i0">
      <p>A year ago, <a href="http://www.villagillet.net">Villa Gillet</a>, an international multidisciplinary center, introduced the <i>Villa</i>, a new typeface, designed by <a href="http://le-combo.fr">le Combo</a> with two styles: the <span>Regular</span>,whose shapes are based on typographic archetypes and the <span><i>Irregular</i></span>, its italic which gets its curves from atypical models that never became standards. Both offer a look on this discipline’s history.</p>
      <p>This year, you are invited to participate in the creation of a style : a bold version, named  <span>Éventuel</span>, whose form  will attest explorations into the future of our alphabet’s construction. Because, if at the scale of a lifetime, our writing system can seem unalterable, it is actually the result of thousands of years of constant transformation. Thus, it is likely that in another thousand years, our letters will have undergone a few mutations!</p>
      <p> It’s your turn now to imagine the future potential steps of this evolution, by drawing the letters of your choice. All the collected proposals will be used as a base for the design of a new typeface: the <span>Villa Éventuel</span>.</p>
    </div>
    <textarea id="commentaire" class="interface i2" placeholder="Write here..." name="name" rows="8" cols="80" required></textarea>

  </div>
</section>

<section id="propositions">

  <div id="listecommentaire" class="interface i2">
    <?php $comz = json_decode(file_get_contents(dirname(dirname(__FILE__))."/data/writting/text.json"), true) ?>
    <?php foreach ($comz as $key => $com): ?>
      <?php $info = json_decode($com, true); ?>
      <div class="commentaire">
        <div class="informations">
          <span class="pseudo"><?php echo $info["nom"]; ?></span>
        le <span class="date"><?php echo $info["date"] ?></span>
        </div>
        <div class="message">
          <?php echo $info["com"] ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <ul class="interface i3">

    <?php foreach ($lettres as $key => $value): ?>
      <?php  $timeline_dir = "../data/timeline/*"?>
      <?php  $drawing_dir = "../data/drawing/*"?>
      <?php $counter = 0; ?>
      <li class='comp'>
        <div class='nomLettre'><?php echo $value ?></div>
        <div style="border-right:none;" class='petitDessins'>
          <div class='innerPetitDessins'>
            <?php foreach (glob($drawing_dir) as $filename): ?>
              <?php
                $info = explode("_",$filename);
                $c = substr(strrchr($info[0], "/"), 1)
              ?>
              <?php if ($c == $value ): ?>
                <img src="<?php echo $filename ?>" alt="">
              <?php endif; ?>
            <?php endforeach; ?>
          </div>
        </div>
        <div class='occurencesLettre'>
          <span></span>
        </div>
        <div class='bouttonDessin' data-lettre="<?php echo $value ?>">Draw</div>
      </li>
      <li class='infoscomp'>
        <div class='titreTimeline'>Evolution of the shapes:</div>


        <div class='timeline'>
        <?php foreach (glob($timeline_dir) as $filename): ?>
          <?php
            $info = explode("_",$filename);
            $c = $info[1];
          ?>
          <?php if ($c == $value ): ?>

            <?php $era = explode(".", $info[2]); ?>
            <div class='lettreTime'>
              <img class="lazy" data-src='<?php echo $filename ?>'/>
              <div class='lazy periodeLettre'><?php echo $era[0] ?><br> <?php echo $era[1] ?></div>
            </div>
            <div class='flecheTimeline'><br><br><br>></div>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>

        <?php foreach (glob($drawing_dir) as $filename): ?>
          <?php
            $info = explode("_",$filename);
            $c = substr(strrchr($info[0], "/"), 1)
          ?>
          <?php if ($c == $value ): ?>
            <?php $counter ++ ?>
          <?php endif; ?>
        <?php endforeach; ?>
        <div class='nbDessin'><?php echo $counter ?> existing <br> drawings</div>
          <div class='dessinPropose'>
            <div class='innerDessinPropose'>
              <?php foreach (glob($drawing_dir) as $filename): ?>
                <?php
                  $info = explode("_",$filename);
                  $c = substr(strrchr($info[0], "/"), 1)
                ?>

                  <?php if ($c == $value): ?>
                    <img class="lazy" data-src="<?php echo $filename ?>" alt="">
                  <?php endif; ?>
              <?php endforeach; ?>
             </div>
           </div>
       </li>
    <?php endforeach; ?>
  </ul>
</section>
</main>
<script type="text/javascript">
  //lazyload
  var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy"
  });
</script>
</body>
</html>
