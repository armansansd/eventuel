# Eventuel (platform)

![](http://bonjourmonde.net/bonjourmonde/user/pages/33.typo_villa_eventuel/images/03.png)

Nous avons créée, avec la Villa Gillet, une plateforme de dessin participative où chacun pouvait imaginer les évolutions futurs de l’alphabet latin. Les contributions ont nourri le dessin de la déclinaison grasse du caractère typographique Villa. Les 2800 dessins ont été présentés sur une bâche lors des Assises Internationales du Roman 2019.

## Getting started

This website use php and paper.js, it should work on any computer with a running LAMP, MAMP, XAMP etc.

### How to use

Simply clone the repository in your server.

## Author

[Bonjour Monde](http://bonjourmonde.net)

## License

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
